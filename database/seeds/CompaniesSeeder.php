<?php

use Illuminate\Database\Seeder;
use App\Models\Company;
use Faker\Factory as Faker;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::truncate();

        $faker = Faker::create();

        for ($i = 0; $i < 100; $i++) {
            Company::create([
                'name' => $faker->company,
                'address' => $faker->address,
                'city' => $faker->city
            ]);
        }
    }
}