<?php

use Illuminate\Database\Seeder;
use App\Models\Stock;
use App\Models\Market;
use Faker\Factory as Faker;


class StocksLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::delete('delete from market_stock');

        $faker = Faker::create();

        $stocks = Stock::all();

        foreach($stocks as $stock) {
            $count = rand(0, 5);

            for ($i = 0; $i < $count; $i++) {
                $stock->markets()->attach($i + 1, [
                    'price' => $faker->randomFloat(2, 1, 1000)
                ]);
            }
        }
    }
}