<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MarketsSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(TypesSeeder::class);
        $this->call(StocksSeeder::class);
        $this->call(StocksLinkSeeder::class);
    }
}
