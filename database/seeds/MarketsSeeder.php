<?php

use Illuminate\Database\Seeder;
use App\Models\Market;

class MarketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Market::truncate();

        Market::create([
            'full_name' => 'New York Stock Exchange',
            'short_name' => 'NYSE'
        ]);

        Market::create([
            'full_name' => 'London Stock Exchange',
            'short_name' => 'LSE'
        ]);

        Market::create([
            'full_name' => 'Hong Kong Stock Exchange',
            'short_name' => 'HKSE'
        ]);

        Market::create([
            'full_name' => 'Shanghai Stock Exchange',
            'short_name' => 'SSE'
        ]);

        Market::create([
            'full_name' => 'Deutsche Börse Frankfurt',
            'short_name' => 'DBF'
        ]);
    }
}