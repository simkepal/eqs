<?php

use Illuminate\Database\Seeder;
use App\Models\Stock;
use App\Models\Company;

class StocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stock::truncate();

        $companies = Company::all();

        foreach($companies as $company) {
            $count = rand(0, 2);

            switch ($count) {
                case 1:
                    $type = rand(1, 2);
                    Stock::create([
                        'type_id' => $type,
                        'company_id' => $company->id
                    ]);
                    break;
                case 2:
                    Stock::create([
                        'type_id' => 1,
                        'company_id' => $company->id
                    ]);
                    Stock::create([
                        'type_id' => 2,
                        'company_id' => $company->id
                    ]);

                    break;

                default:
                    # code...
                    break;
            }
        }
        
    }
}