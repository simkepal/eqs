<?php

use Illuminate\Database\Seeder;
use App\Models\StockType;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StockType::truncate();

        StockType::create(['name' => 'Common']);
        StockType::create(['name' => 'Preferred']);
    }
}