<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StockType;
use App\Http\Requests\StockTypeStoreRequest;

class StockTypesController extends Controller
{
    public function create()
    {
        return view("stock_types.create");
    }

    public function store(StockTypeStoreRequest $request)
    {
        StockType::create(request()->only(['name']));

        session()->flash('message', 'Success! Stock Type added');

        return redirect('companies');
    }
}
