<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Stock;
use App\Models\StockType;
use App\Models\Market;
use App\Http\Requests\StockStoreRequest;
use App\Repositories\Stocks;
use App\Http\Requests\StockUpdateRequest;
use DB;

class StocksController extends Controller
{

    protected $repository;

    public function __construct(Stocks $repository) {
        $this->repository = $repository;
    }

    public function index()
    {
        $markets = Market::all();

        $data = [];

        foreach($markets as $market) {
            $stock = $market->stocks()->with('markets')->orderBy('price', 'DESC')->first();

            if ($stock) {
                $data[] = [
                    'stock' => $stock,
                    'market' => $market
                ];
            }
        }

        return view('stocks.index', compact('data'));
    }
    
    public function create(Company $company)
    {
        $alreadyCreated = $company->stocks()->pluck('type_id')->toArray();
        $types = StockType::whereNotIn('id', $alreadyCreated)->get();

        if ($types->count() == 0) {
            session()->flash('redirect_message', 'You where redirected here because there are no stock types or you already created stocks for all stock types.');
            return redirect()->route('create_stock_type');
        }

        return view('stocks.create', compact('company', 'types'));
    }

    public function store(StockStoreRequest $request, Company $company)
    {
        $this->repository->addStock($company, request('type'));

        session()->flash('message', 'Success! Stock added');

        return redirect()->route('show_company', [$company]);
    }

    public function edit(Stock $stock, $market = false)
    {
        if ($market) {
            $market = $stock->markets()->where('id', $market)->first();

            if ($market) {
                return view('stocks.edit', compact('stock', 'market'));
            }
        }

    
        $alreadyLinked = $stock->markets()->pluck('id')->toArray();
        $markets = Market::whereNotIn('id', $alreadyLinked)->get();

        if ($markets->count() == 0) {
            return back();
        }

        return view('stocks.edit', compact('stock', 'markets'));
    }

    public function update(StockUpdateRequest $request, Stock $stock)
    {
        $marketId = request('market');
        $price = request('price');

        $market = $stock->markets()->where('id', $marketId)->first();

        if ($market) {
            $stock->markets()->updateExistingPivot($market->id, ['price' => $price]);
        }
        else {
            $market = Market::find($marketId);

            $stock->markets()->attach($market, ['price' => $price]);
        }

        session()->flash('message', 'Success! Stock updated');

        return redirect('/markets/' . $market->id);
    }
} 