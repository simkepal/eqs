<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Market;
use App\Http\Requests\MarketStoreRequest;

class MarketsController extends Controller
{
    
    public function index()
    {
        $markets = Market::all();

        return view('markets.index', compact('markets'));
    }

    public function create()
    {
        return view('markets.create');
    }

    public function store(MarketStoreRequest $request)
    {
        Market::create(request()->only(['full_name', 'short_name']));

        session()->flash('message', 'Success! Market added');

        return redirect('markets');
    }

    public function show(Market $market) 
    {
        $stocks = $market->stocks()->with(['company', 'type'])->orderBy('market_stock.updated_at', 'desc')->get();

        return view('markets.show', compact('market', 'stocks'));
    }

    public function edit(Market $market)
    {
        return view('markets.edit', compact('market'));
    }

    public function update(MarketStoreRequest $request, Market $market)
    {
        $market->update(request()->only(['full_name', 'short_name']));

        session()->flash('message', 'Success! Market updated');
        
        return redirect()->route('show_market', [$market]);
    }

    public function destroy(Market $market)
    {
        $market->stocks()->detach();
        $market->delete();

        session()->flash('message', 'Success! Market deleted');

        return redirect('markets');
    }
}
