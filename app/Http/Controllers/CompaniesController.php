<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Http\Requests\CompanyStoreRequest;

class CompaniesController extends Controller
{
    public function index()
    {
        $companies = Company::orderBy('name')->get();

        return view('companies.index', compact('companies'));
    }

    public function create()
    {
        return view('companies.create');
    }

    public function store(CompanyStoreRequest $request)
    {
        Company::create(request()->only(['name', 'address', 'city']));

        session()->flash('message', 'Success! Company added');

        return redirect('companies');
    }

    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    public function edit(Company $company)
    {
        return view('companies.edit', compact('company'));
    }

    public function update(CompanyStoreRequest $request, Company $company)
    {
        $company->update(request()->only(['name', 'address', 'city']));

        session()->flash('message', 'Success! Company updated');
        
        return redirect()->route('show_company', [$company]);
    }

    public function destroy(Company $company)
    {
        $company->stocks()->delete();
        $company->delete();

        session()->flash('message', 'Success! Company deleted');

        return redirect('companies');
    }
}
