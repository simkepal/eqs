<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['type_id', 'company_id'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function type()
    {
        return $this->belongsTo(StockType::class);
    }

    public function markets()
    { 
        return $this->belongsToMany(Market::class)->withPivot('price')->withTimestamps();
    }        
}
