<?php

namespace App\Models;

class Market extends Model
{
    protected $fillable = ['full_name', 'short_name'];

    public function stocks()
    { 
        return $this->belongsToMany(Stock::class)->withPivot('price')->withTimestamps();
    }
}
