<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Stocks;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['home.index', 'layouts.highest'], function($view) {
            $view->with('highest', Stocks::getHighest());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
