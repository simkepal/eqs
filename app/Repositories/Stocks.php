<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\Stock;
use App\Models\Market;

class Stocks
{
    public function addStock(Company $company, $type_id)
    {
        //check if stock exists
        $exists = $company->stocks()
            ->where('type_id', $type_id)
            ->exists();

        // if stock does not exists create it
        if (!$exists) {
            $company->stocks()->create(compact('type_id'));
        }
    }

    public static function getHighest()
    {
       
        $stocks = Stock::join('market_stock', 'market_stock.stock_id', '=', 'stocks.id')
            ->select('*')
            ->orderBy('market_stock.price', 'DESC')
            ->take(3)
            ->get();
            
        return $stocks;
    }

}