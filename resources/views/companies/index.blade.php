@extends('layouts.master')

@section('content')
    <div class="blog-header">
        <h1 class="blog-title">Companies</h1>
        <p class="lead blog-description">Select Company from list to see the details</p>
    </div>

    <hr>

    <div class="row">

        @if($companies->count() > 0)
            <div class="col-sm-8 blog-main">
                <ul>
                    @foreach($companies as $company)
                        <li>
                            <a href="/companies/{{ $company->id }}">{{ $company->name }}</a>
                        </li>
                    @endforeach()
                </ul>
            </div>
        @else 
            <div class="col-sm-8 blog-main">
                <h4>There are no companies yet. Add a company with button on the right.</h4>
            </div>
        @endif


        <div class="col-sm-3 offset-sm-1 blog-sidebar">
            <a href="/companies/create" class="btn btn-success">Add New Company</a>
            @include ('layouts.highest')
        </div>
    </div>
@endsection