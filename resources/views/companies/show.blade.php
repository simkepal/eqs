@extends('layouts.master')

@section('content')

    <div class="clear-both">
        <div class="blog-header">
            <div class="float-left">
                <h1>{{ $company->name }}</h1>
                <h5>{{ $company->address }}, {{ $company->city }}</h5>
            </div>

            <form class="float-right" action="/companies/{{ $company->id }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="submit" class="btn btn-danger ">Delete</button>
                    <a href="/companies/{{ $company->id }}/edit" class="btn btn-primary">Edit</a>
                    <a href="/companies/{{ $company->id }}/stocks/create" class="btn btn-success">Add Stock</a>
                </div>
            </form>
        </div>
    </div>


    <div class="clearfix"></div>
    
    <hr>

    @if($company->stocks()->count())
    <div class="card-deck">
        @foreach($company->stocks()->get() as $stock)
            @include ('stocks.card')
        @endforeach
    </div>
    @else
        <h4>There are no Stocks added yet.</h4>
    @endif     


        

@endsection