@extends('layouts.master')

@section('content')
    <div class="blog-header">
        <h1 class="blog-title">Companies</h1>
        <p class="lead blog-description">Create new Company by filling the form bellow.</p>
    </div>

    <hr>

    <div class="row">

        <div class="col-sm-8 blog-main">
            <form action="/companies" method="POST" >

                {{ csrf_field() }}

                <div class="form-group {{ $errors->first('name') ? 'has-danger' : '' }}">
                    <label for="name">Company Name</label>
                    <input required type="text" class="form-control" id="name" name="name" placeholder="Enter Company Name" value="{{ old('name') }}">
                </div>

                <div class="form-group {{ $errors->first('address') ? 'has-danger' : '' }}">
                    <label for="address">Company Address</label>
                    <input required type="text" class="form-control" id="address" name="address" placeholder="Enter Company Address" value="{{ old('address') }}" >
                </div>

                <div class="form-group {{ $errors->first('city') ? 'has-danger' : '' }}">
                    <label for="city">Company City</label>
                    <input required type="text" class="form-control" id="city" name="city" placeholder="Enter Company City" value="{{ old('city') }}">
                </div>
            
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>


            @include('layouts.errors')
        </div>

    </div>
@endsection