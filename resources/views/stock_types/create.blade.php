@extends('layouts.master')

@section('content')
    <div>
        <h1>Add New Stock Type</h1>

        @if($redirect_message = session('redirect_message'))
            <div class="alert alert-info" role="alert">
                {{ $redirect_message }}
            </div>
        @endif
    </div>

    <hr>

    <div class="row">

        <div class="col-sm-8">
            <form action="/stock-types" method="POST" >

                {{ csrf_field() }}

                <div class="form-group {{ $errors->first('name') ? 'has-danger' : '' }}">
                    <label for="name">Stock Type Name</label>
                    <input required type="text" class="form-control" id="name" name="name" placeholder="Enter Stock Type Name" value="{{ old('name') }}">
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.errors')
        </div>

    </div>
@endsection