@extends('layouts.master')

@section('content')
    <div class="blog-header">
        <h1 class="blog-title">Edit Stock {{ $stock->company->name }}</h1>
        <p>{{ $stock->type->name }}</p>
    </div>

    <hr>

    <div class="row">

        <div class="col-sm-8 blog-main">
            <form action="/stocks/{{ $stock->id }}" method="POST">
                {{ method_field('PATCH') }}

                {{ csrf_field() }}

                @if(isset($markets)) 
                <div class="form-group {{ $errors->first('market') ? 'has-danger' : '' }}">
                    <label for="market">Select Market for Stock</label>
                    <select required name="market" class="form-control" id="type">
                        <option></option>
                        @foreach($markets as $market) 
                            <option value="{{ $market->id }}">{{ $market->full_name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif

                @if(isset($market)) 
                <input type="hidden" value="{{ $market->id }}" name="market">
                @endif

                <div class="form-group {{ $errors->first('price') ? 'has-danger' : '' }}">
                    <label for="price">Stock price on market</label>
                    <input required value="{{ $market ? '' : number_format($market->pivot->price, 2) }}" type="text" class="form-control" id="price" name="price" placeholder="Enter Stock price on market">
                </div>
               
            
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.errors')
        </div>

    </div>
@endsection