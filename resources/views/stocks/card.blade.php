<div class="card {{ $stock->type->id % 2 == 0 ? 'card-outline-info' :'card-outline-warning' }}">
    <div class="card-block">
        <h4 class="card-title">{{ $company->name }}</h4>
        <p class="card-text">{{ $stock->type->name }}</p>
    </div>

    @if($stock->markets->count() > 0)
    <ul class="list-group">
        @foreach($stock->markets as $market)
            <li class="list-group-item list-group-item justify-content-between">
                {{ $market->full_name }}
                <a href="/stocks/{{ $stock->id }}/edit/{{ $market->id }}">
                    <span class="badge badge-default badge-pill big-badge">{{ number_format($market->pivot->price,2) }} €</span>
                </a>
            </li>
        @endforeach
    </ul>
    @else
        <div class="card-block">This Stock is not linked to any Stock Market</div>
    @endif
    <div class="card-block">
        <a href="/stocks/{{ $stock->id }}/edit" class="card-link">Add To Market</a>
    </div>
</div>