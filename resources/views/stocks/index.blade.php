@extends('layouts.master')

@section('content')
    <div class="blog-header">
        <h1 class="blog-title">Highest Market Prices</h1>
    </div>

     <hr>


    <div class="row">

        @if(count($data) > 0)
            @foreach($data as $d)
                <div class="col-sm-4 card-deck">
                    <div class="card mb-3 {{ $d['stock']->type->id % 2 == 0 ? 'card-outline-info' :'card-outline-warning' }}">
                        <div class="card-block">
                            <h4 class="card-title">{{ $d['stock']->company->name }}</h4>
                            <p class="card-text">{{ $d['stock']->type->name }}</p>
                        </div>
                        <div class="card-block">
                            <h2>{{ number_format($d['stock']->pivot->price, 2) }} €</h2>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">{{ $d['market']->full_name }}</small>
                        </div>
                    </div>
                </div>
            @endforeach
        @else 
            <h4>There are no stocks added yet to monitor.</h4>
        @endif
    </div>

@endsection