@extends('layouts.master')

@section('content')
    <div>
        <h1>Add New Stock for "{{ $company->name }}"</h1>
        <p class="leas">Create new Stock by filling the form bellow.</p>
    </div>

    <hr>

    <div class="row">

        <div class="col-sm-8">
            <form action="/companies/{{ $company->id }}/stocks" method="POST" >

                {{ csrf_field() }}

                <div class="form-group {{ $errors->first('type') ? 'has-danger' : '' }}">
                    <label for="type">Type</label>
                    <select required name="type" class="form-control" value="{{ old('type') }}" id="type">
                        <option></option>
                        @foreach($types as $type) 
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.errors')
        </div>

        <div class="col-sm-3 offset-sm-1">

            Do you need a new Stock Type?
            <a href="/stock-types/create" class="btn btn-success">New Stock Type</a>
        </div>

    </div>
@endsection