@extends('layouts.master')

@section('content')

    <div class="blog-header">
        <div class="float-left">
            <h1 class="blog-title">
                {{ $market->full_name }}
                <small>({{ $market->short_name }})</small>
            </h1>
        </div>
        <a href="/markets/{{ $market->id }}/edit" class="btn btn-success float-right">Edit</a>
        
        <form class="float-right" action="/markets/{{ $market->id }}" method="POST">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary ">Delete</button>
        </form>
    </div>

    @if($stocks->count() > 0)
    <table class="table">
        <thead class="thead-default">
            <tr>
                <th>#</th>
                <th>Company</th>
                <th>Stock Type</th>
                <th>Added</th>
                <th>Updated</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach($stocks as $key => $stock)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>
                    <a href="/companies/{{ $stock->company->id }}">
                        {{ $stock->company->name }}
                    </a>
                </td>

                <td>{{ $stock->type->name }}</td>
                <td>{{ $stock->pivot->created_at->diffForHumans() }}</td>
                <td>{{ $stock->pivot->updated_at->diffForHumans() }}</td>
                <td>
                    <a href="/stocks/{{ $stock->id }}/edit/{{ $market->id }}">
                        <span class="badge badge-default badge-pill big-badge">{{ number_format($stock->pivot->price,2) }} €</span>
                    </a>
                </td>
            </tr>
           @endforeach
        </tbody>
    </table>
    @else
        <div class="clearfix"></div>
        <hr>
        <h4>There are no Stocks added yet. Add a company and add stock <a href="/companies">here</a>.</h4>
    @endif

@endsection