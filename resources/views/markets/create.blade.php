@extends('layouts.master')

@section('content')
    <div class="blog-header">
        <h1 class="blog-title">Markets</h1>
        <p class="lead blog-description">Create new market by filling the form bellow.</p>
    </div>

    <hr>

    <div class="row">

        <div class="col-sm-8 blog-main">
            <form action="/markets" method="POST" >

                {{ csrf_field() }}

                <div class="form-group {{ $errors->first('full_name') ? 'has-danger' : '' }}">
                    <label for="full_name">Market Full Name</label>
                    <input required value="{{ old('full_name') }}" type="text" class="form-control" id="full_name" name="full_name" placeholder="Enter Market Full Name">
                </div>
                <div class="form-group {{ $errors->first('short_name') ? 'has-danger' : '' }}">
                    <label for="short_name">Market Short Name</label>
                    <input required value="{{ old('short_name') }}" type="text" class="form-control" id="short_name" name="short_name" placeholder="Enter Market Short Name">
                </div>
            
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.errors')
        </div>

    </div>
@endsection