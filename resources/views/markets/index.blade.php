@extends('layouts.master')

@section('content')
    <div class="blog-header">
        <h1 class="blog-title">Markets</h1>
        <p class="lead blog-description">Select Market from list to see the stocks</p>
    </div>

    <hr>

    <div class="row">

        @if($markets->count() > 0)
            <div class="col-sm-8 blog-main">
                <ul>
                    @foreach($markets as $market)
                        <li>
                            <a href="/markets/{{ $market->id }}">{{ $market->full_name }}</a>
                        </li>
                    @endforeach()
                </ul>
            </div>
        @else 
            <div class="col-sm-8 blog-main">
                <h4>There are no markets yet. Add a market with button on the right.</h4>
            </div>
        @endif


        @include ('layouts.highest')
    </div>
@endsection