@extends('layouts.master')

@section('content')
    <div class="jumbotron">
        <h1>Bills App</h1>
        <p class="lead">Welcome to Bills App of Stock Market Exchange! </p>
        <a class="btn btn-lg btn-success" href="/markets" role="button">Stock Markets</a>
        <a class="btn btn-lg btn-primary" href="/companies" role="button">Companies</a>
        <a class="btn btn-lg btn-info" href="/stocks" role="button">Stocks</a>
    </div>

    <h1 class="text-center">Three currently highest stocks we monitor</h1>
    <hr>
    <div class="row">
        @if($highest->count() > 0)
            @foreach($highest as $stock)
                <div class="col-lg-4 text-center">
                    <h4>{{ $stock->company->name}}</h4>
                    <h2>{{ number_format($stock->price, 2) }} €</h2>
                    <p>
                        <a class="btn btn-primary" href="/companies/{{ $stock->company_id }}" role="button">View details</a>
                    </p>
                </div>
            @endforeach
        @endif
    </div>
@endsection()