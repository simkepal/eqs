@if($message = session('message'))
    <div class="alert alert-success message-alert" role="alert">
        {{ $message }}
    </div>
@endif