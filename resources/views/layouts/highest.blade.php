@if($highest->count() > 0)
<div class="sidebar-module sidebar-module-inset">
    <h4>Three currently highest stocks we monitor</h4>
</div>
<hr>
<div class="sidebar-module sidebar-module-inset">
    @foreach($highest as $stock)
        <a href="/companies/{{ $stock->company->id }}">
            <h6>{{ $stock->company->name }}</h6>
            <h3>{{ $stock->price}} € </h3>
        </a>
        <hr>
    @endforeach
</div>
@endif
