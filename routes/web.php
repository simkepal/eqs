<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/markets', 'MarketsController@index')->name('markets');
Route::get('/markets/create', 'MarketsController@create');
Route::post('/markets', 'MarketsController@store');
Route::get('/markets/{market}', 'MarketsController@show')->name('show_market');
Route::get('/markets/{market}/edit', 'MarketsController@edit');
Route::patch('/markets/{market}', 'MarketsController@update');
Route::delete('/markets/{market}', 'MarketsController@destroy');

Route::get('/companies', 'CompaniesController@index')->name('companies');
Route::get('/companies/create', 'CompaniesController@create');
Route::post('/companies', 'CompaniesController@store');
Route::get('/companies/{company}', 'CompaniesController@show')->name('show_company');
Route::get('/companies/{company}/edit', 'CompaniesController@edit');
Route::patch('/companies/{company}', 'CompaniesController@update');
Route::delete('/companies/{company}', 'CompaniesController@destroy');

Route::get('/companies/{company}/stocks/create', 'StocksController@create');
Route::post('/companies/{company}/stocks', 'StocksController@store');

Route::get('/stocks', 'StocksController@index');
Route::get('/stocks/{stock}/edit/{company_id?}', 'StocksController@edit');
Route::patch('/stocks/{stock}', 'StocksController@update');

Route::get('/stock-types/create', 'StockTypesController@create')->name('create_stock_type');
Route::post('/stock-types', 'StockTypesController@store');
